import React from "react";

import { Text, Center, NativeBaseProvider, Flex } from "native-base";

export default function App() {
  return (
    <NativeBaseProvider>
      <Center flex={1}>
        <Center
          bg="primary.400"
          _text={{
            color: "white",
            fontWeight: "bold",
          }}
          height="32"
          width="40"
          shadow={2}
        >
          <Text>Cybergrill-mobile</Text>
        </Center>
      </Center>
    </NativeBaseProvider>
  );
}
